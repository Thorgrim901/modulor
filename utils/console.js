var colors = require('colors');

module.exports = {
  printDroids: function () {
    console.log(
      "                                  /~\            \n".yellow.bold +
      "                                 |oo )           \n".yellow.bold +
      "                                 _\\=/_           \n".yellow.bold +
      "                 ___            /  _  \\          \n".yellow.bold +
      "                / ()\\          //|/.\\|\\\\         \n".yellow.bold +
      "              _|_____|_        \\\\ \\_/  ||        \n".yellow.bold +
      "             | | === | |        \\|\\ /| ||        \n".yellow.bold +
      "             |_|  O  |_|        # _ _/ #         \n".yellow.bold +
      "              ||  O  ||          | | |           \n".yellow.bold +
      "              ||__*__||          | | |           \n".yellow.bold +
      "             |~ \\___/ ~|         []|[]           \n".yellow.bold +
      "             /=\\ /=\\ /=\\         | | |           \n".yellow.bold +
      "  ___________[_]_[_]_[_]________/_]_[_\\_____________\n".yellow.bold);
  },
  printTitle: function () {
    console.log(
      "  ___  ___ _____ ______  _   _  _      _____ ______ \n".red.bold +
      "  |  \\/  ||  _  ||  _  \\| | | || |    |  _  || ___ \\\n".red.bold +
      "  | .  . || | | || | | || | | || |    | | | || |_/ /\n".red.bold +
      "  | |\\/| || | | || | | || | | || |    | | | ||    / \n".red.bold +
      "  | |  | |\\ \\_/ /| |/ / | |_| || |____\\ \\_/ /| |\\ \\ \n".red.bold +
      "  \\_|  |_/ \\___/ |___/   \\___/ \\_____/ \\___/ \\_| \\_|\n".red.bold);
  }
};