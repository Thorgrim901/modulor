angular.module('#{moduleName}.translations', [
    #{translationsModules}
  ])
  .config([
    '$translateProvider', #{translationsConstants},
    function ($translateProvider, #{translationsConstantsInjections}) {
      #{translationProviderRows}
    }
  ]);