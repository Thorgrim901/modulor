angular.module('app.resources.#{moduleName}', [])
  .factory('#{className}', [
    '$resource',
    function ($resource) {

      var resource = $resource(AppConfig.networkUrl + '/api/#{pluralizedName}/:id', null, {
        update: {
          method: 'PUT',
          params: {
            id: "@id"
          }
        }
      });

      return resource;
    }
  ]);