angular.module('#{moduleName}', [])
  .config([
    '$stateProvider',
    function ($stateProvider) {
      $stateProvider
        .state('#{stateName}', {
          url: '/#{viewName}',
          templateUrl: '#{viewPath}/#{viewName}.html',
          controller: '#{controllerName}'
        });
    }
  ])
  .controller('#{controllerName}', [
    '$scope',
    function ($scope) {

    }
  ]);