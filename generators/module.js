var path = require('path');
var fs = require('fs');
var changeCase = require('change-case');
var _ = require('lodash');
var mkdirp = require('mkdirp');
var colors = require('colors');

function execute() {

  var moduleName = process.argv[3];

  if (moduleName[moduleName.length - 1] === '.') {
    console.log('Incorrect module name');
    return;
  }

  var moduleParts = moduleName.split('.');

  var phisicalPath = path.join(process.cwd(), 'src', moduleParts.join('/'));
  moduleParts.shift();
  var viewPath = moduleParts.join('/');

  var mainJS = fs.readFileSync(path.join(__dirname, "../templates/module/main.js")).toString();

  var fileName = moduleParts[moduleParts.length - 1];

  var controllerName = _.map(moduleParts, function (p) {
    return changeCase.pascalCase(p);
  }).join("") + "Controller";

  var stateName = moduleParts.join(".");

  mainJS = mainJS.replace(/#{moduleName}/g, moduleName);
  mainJS = mainJS.replace(/#{viewPath}/g, viewPath);
  mainJS = mainJS.replace(/#{viewName}/g, fileName);
  mainJS = mainJS.replace(/#{controllerName}/g, controllerName);
  mainJS = mainJS.replace(/#{stateName}/g, stateName);

  if (!fs.existsSync(phisicalPath)) {
    mkdirp.sync(phisicalPath);
  }
  var filePath = path.join(phisicalPath, fileName);

  fs.writeFileSync(filePath + ".js", mainJS);
  fs.writeFileSync(filePath + ".pug", "#" + moduleParts.join("-"));

  console.log(("  Generated module '" + moduleName + "':").green.bold);
  console.log("    " + "Js path".yellow.bold + ":\t" + filePath + ".js");
  console.log("    " + "View path".yellow.bold + ":\t" + filePath + ".pug");
}

module.exports = {
  execute: execute
};