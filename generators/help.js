var colors = require('colors');

module.exports = {
  execute: function () {
    console.log("  modulor module <moduleName>".green.bold + "\n    Generates an angular module with the specified name");
    console.log("");
    console.log("  modulor resource <resourceName>".green.bold + "\n    Generates an angular resource with the specified name");
    console.log("");
    console.log("  modulor translations <moduleName> [<lang>]".green.bold + "\n    Generates the translations for each of the specified " +
      "languages for the specified module.\n    Example:\n      modulor translations app.module1 it-IT en-US");
  }
};