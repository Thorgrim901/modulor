var path = require('path');
var fs = require('fs');
var changeCase = require('change-case');
var _ = require('lodash');
var mkdirp = require('mkdirp');
var pluralize = require('pluralize');
var colors = require('colors');

function execute() {
  var file = fs.readFileSync(path.join(__dirname, "../templates/resource/resource.js")).toString();

  var moduleName = process.argv[3];
  var pluralizedName = pluralize(moduleName);
  var className = changeCase.pascalCase(moduleName);

  file = file.replace(/#{moduleName}/g, moduleName);
  file = file.replace(/#{pluralizedName}/g, pluralizedName);
  file = file.replace(/#{className}/g, className);

  var directory = path.join(process.cwd(), 'src/app/resources');

  if (!fs.existsSync(directory)) {
    mkdirp.sync(directory);
  }

  var resourceFilePath = path.join(directory, "resources.js");

  fs.writeFileSync(path.join(directory, moduleName + ".js"), file);
  if (fs.existsSync(resourceFilePath)) {
    fs.unlinkSync(resourceFilePath);
  }

  var files = fs.readdirSync(directory);

  files = _.map(files, function (f) {
    var extension = path.extname(f);
    var baseName = path.basename(f, extension);
    return "'app.resources." + baseName + "'";
  });

  var allResources = files.join(",\n  ");
  var resources = fs.readFileSync(path.join(__dirname, "../templates/resource/all.js")).toString();
  resources = resources.replace(/#{allResources}/g, allResources);

  fs.writeFileSync(resourceFilePath, resources);

  console.log(("  Generated resource '" + moduleName + "'").green.bold);
}

module.exports = {
  execute: execute
};