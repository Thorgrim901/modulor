var path = require('path');
var fs = require('fs');
var changeCase = require('change-case');
var _ = require('lodash');
var mkdirp = require('mkdirp');
var pluralize = require('pluralize');
var colors = require('colors');

function execute() {
  var moduleName = process.argv[3];
  var langs = process.argv.slice(4, process.argv.length);

  var mainJS = fs.readFileSync(path.join(__dirname, "../templates/translations/all.js")).toString();
  var moduleParts = moduleName.split('.');
  var phisicalPath = path.join(process.cwd(), "src", moduleParts.join("/"));

  if (!fs.existsSync(phisicalPath)) {
    mkdirp.sync(phisicalPath);
  }

  mainJS = mainJS.replace(/#{moduleName}/g, moduleName);

  var constantsBuilder = [];
  var translationsModules = [];
  var providerBuilder = [];
  _.each(langs, function (pureLang) {
    var lang = pureLang.replace(/-/g, "");
    var constant = _.map(moduleParts, function (m) {
      return changeCase.pascalCase(m);
    }).join("") + changeCase.pascalCase(lang);
    constantsBuilder.push(constant);
    providerBuilder.push("$translateProvider.translations('" + pureLang + "', " + constant + ");");

    translationsModules.push(moduleName + ".translations." + lang);

    var translationFile = fs.readFileSync(path.join(__dirname, "../templates/translations/translation.js")).toString();

    translationFile = translationFile.replace(/#{moduleName}/g, moduleName);
    translationFile = translationFile.replace(/#{lang}/g, lang);
    translationFile = translationFile.replace(/#{constantName}/g, constant);
    var filePath = path.join(phisicalPath, "translation_" + lang + ".js");

    fs.writeFileSync(filePath, translationFile);
  });

  var translationsConstants = _.map(constantsBuilder, function (c) {
    return "'" + c + "'";
  }).join(", ");
  translationsModules = _.map(translationsModules, function (c) {
    return "'" + c + "'";
  }).join(",\n    ");

  mainJS = mainJS.replace(/#{translationProviderRows}/g, providerBuilder.join("\n      "));
  mainJS = mainJS.replace(/#{translationsConstants}/g, translationsConstants);
  mainJS = mainJS.replace(/#{translationsModules}/g, translationsModules);
  mainJS = mainJS.replace(/#{translationsConstantsInjections}/g, constantsBuilder.join(", "));

  fs.writeFileSync(path.join(phisicalPath, "translations.js"), mainJS);

  console.log(("  Generated translations for '" + moduleName + "'").green.bold);
  console.log(("    " + langs.join(", ")).yellow.bold);
}

module.exports = {
  execute: execute
};