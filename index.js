#!/usr/bin/env node

var consolePrinter = require('./utils/console');
consolePrinter.printTitle();

var generatorName = process.argv[2];

var generator;
try {
  generator = require('./generators/' + generatorName);
} catch (e) {
  if (e.code === 'MODULE_NOT_FOUND') {
    consolePrinter.printDroids();
    console.log(
      "  This is not the generator you are looking for.\n" +
      "           Use 'modulor help' for help.");
    return;
  }
}

generator.execute();